import {
  BadRequestException,
  Controller,
  Get,
  Query,
  UseGuards,
} from "@nestjs/common";
import { ApiService } from "./api.service";
import JwtGuard from "../auth/guards/jwt.guard";
import { User } from "../user/decorators/user.decorator";
import { UserService } from "../user/user.service";

@Controller("api")
export class ApiController {
  constructor(
    private readonly apiService: ApiService,
    private readonly userService: UserService
  ) {}

  @Get("week")
  @UseGuards(JwtGuard)
  async getWeek(@User() userToken, @Query() query) {
    //query.time - timestamp понедельника
    if (!query.time)
      throw new BadRequestException("Не указан query параметр time");

    const user = await this.userService.findById(userToken._id);
    const firstHalf = await this.apiService.getWeekHalf(
      user.eduToken,
      query.time
    );
    const secondHalf = await this.apiService.getWeekHalf(
      user.eduToken,
      Number(query.time) + 345600
    );
    return [...firstHalf, ...secondHalf];
  }
}
