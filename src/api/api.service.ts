import fetch from "node-fetch";
import * as cookie from "cookie";
import * as crypto from "crypto";
import { HTMLElement, parse } from "node-html-parser";

import { Injectable, UnauthorizedException } from "@nestjs/common";
import { SignInDto } from "../auth/dto/signin.dto";
import { IEduLogin } from "./interfaces/edu-login.interface";

@Injectable()
export class ApiService {
  constructor() {}

  async eduLogin(signInDto: SignInDto): Promise<IEduLogin> {
    const hash = crypto
      .createHash("sha1")
      .update("signInDto.login.toString()" + Date.now())
      .digest("hex");
    const res = await fetch("https://edu.tatar.ru/logon", {
      method: "POST",
      mode: "no-cors",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Host: "edu.tatar.ru",
        Origin: "https://edu.tatar.ru",
        Referer: "https://edu.tatar.ru/logon",
        Cookie: `DNSID=${hash}`,
      },
      body: `main_login2=${signInDto.login}&main_password2=${signInDto.password}`,
    });

    try {
      const token = cookie.parse(res.headers.get("set-cookie")).DNSID;

      const table = parse(await res.text())
        .querySelector(".tableEx")
        .querySelectorAll("td");

      const login = Number(table[3].innerText);
      const name = table[1].innerText.split(" ").slice(0, 2).join(" ");
      const school = table[5].innerText;

      return {
        eduToken: token,
        login,
        name,
        school,
      };
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  async getUserClass(eduToken: string): Promise<string> {
    const res = await fetch("https://edu.tatar.ru/user/diary/week", {
      method: "GET",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Host: "edu.tatar.ru",
        Origin: "https://edu.tatar.ru",
        Referer: "https://edu.tatar.ru/",
        Cookie: `DNSID=${eduToken}`,
      },
    });

    if (res.redirected) {
      //TODO: обновлять токен, если сессия на edu.tatar.ru устарела
      throw new UnauthorizedException();
    }

    const body = await res.text();
    return parse(body)
      .querySelector(".top-panel-user")
      .querySelector("span")
      .innerText.split(",\n")[1]
      .replace("\t\t\t\t", "");
  }

  //time - unix timestamp первого дня полунедели
  async getWeekHalf(eduToken: string, time?: number) {
    const res = await fetch(
      `https://edu.tatar.ru/user/diary/week${time ? `?date=${time}` : ""}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Host: "edu.tatar.ru",
          Origin: "https://edu.tatar.ru",
          Referer: "https://edu.tatar.ru/",
          Cookie: `DNSID=${eduToken}`,
        },
      }
    );

    if (res.redirected) {
      //TODO: обновлять токен, если сессия на edu.tatar.ru устарела
      console.log("wtf");
      throw new UnauthorizedException();
    }

    const body = await res.text();
    let weekHalf = [[], [], []];

    const tableArray = parse(body)
      .querySelector("table")
      .querySelectorAll("tr");

    let weekDayIndex = 0;

    tableArray.forEach((tr, i) => {
      //В каждом дне недели 8 полей [предмет, ДЗ, оценка], отделяем день недели
      // с помощью <tr class="tt-separator">
      if (tr.classList.contains("tt-separator")) {
        weekDayIndex += 1;
        return;
      }

      //Наполняем день недели записями
      let tds = tr.childNodes;

      //Удаляем елементы <tr class="tt-days">, так как они содержат дату и сортируем по типу
      let name: string, homeTask: string, mark: string;
      tds.forEach((td) => {
        if ((td as HTMLElement).classList?.contains("tt-subj")) {
          name = td.innerText.replace(/[\r\n\t]/gm, "");
        } else if ((td as HTMLElement).classList?.contains("tt-task")) {
          homeTask = td.innerText.replace(/[\r\n\t]/gm, "");
        } else if ((td as HTMLElement).classList?.contains("tt-mark")) {
          mark = td.innerText.replace(/[\r\n\t]/gm, "");
        }
      });

      if (name || homeTask || mark)
        weekHalf[weekDayIndex].push({
          name,
          homeTask,
          mark,
        });
    });

    return weekHalf;
  }
}
