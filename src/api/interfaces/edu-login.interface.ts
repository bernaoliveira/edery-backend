export interface IEduLogin {
  eduToken: string;
  login: number;
  name: string;
  school: string;
}
