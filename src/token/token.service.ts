import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IUserToken } from "./interfaces/token.interface";
import { CreateUserTokenDto } from "./dto/create-user-token.dto";
@Injectable()
export class TokenService {
  constructor(
    @InjectModel("Token") private readonly tokenModel: Model<IUserToken>
  ) {}

  async create(createUserTokenDto: CreateUserTokenDto): Promise<IUserToken> {
    const userToken = new this.tokenModel(createUserTokenDto);
    return await userToken.save();
  }

  async delete(
    uid: string,
    token: string
  ): Promise<{ ok?: number; n?: number }> {
    return this.tokenModel.deleteOne({ uid, token });
  }

  async deleteAll(uid: string): Promise<{ ok?: number; n?: number }> {
    return this.tokenModel.deleteMany({ uid });
  }

  async exists(uid: string, token: string): Promise<boolean> {
    return await this.tokenModel.exists({ uid, token });
  }
}
