import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SchemaDocument = Token & Document;

@Schema()
export class Token {
  @Prop({
    required: true,
  })
  token: string;

  @Prop({
    required: true,
  })
  uid: string;

  @Prop({
    required: true,
  })
  expiresAt: string;
}

export const TokenSchema = SchemaFactory.createForClass(Token);
