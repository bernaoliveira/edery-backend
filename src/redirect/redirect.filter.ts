import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from "@nestjs/common";
import { RedirectException } from "./redirect.exception";

@Catch()
export class RedirectFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const res = host.switchToHttp().getResponse();
    try {
      if (exception instanceof RedirectException) {
        res.redirect(exception.message);
      }
    } catch (e) {
      return res.status(500).json({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: e.message,
      });
    }
  }
}
