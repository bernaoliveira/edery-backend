import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";

import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { JwtStrategy } from "./jwt.strategy";
import { TokenModule } from "src/token/token.module";
import { UserModule } from "src/user/user.module";
import { ApiModule } from "../api/api.module";

@Module({
  imports: [
    TokenModule,
    PassportModule.register({ defaultStrategy: "jwt" }),
    JwtModule.register({
      secret: process.env.JWT_KEY,
      signOptions: { expiresIn: "14d" },
    }),
    UserModule,
    ApiModule
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
