import {IsNotEmpty, IsNumber, IsString} from 'class-validator';

export class SignInDto {
  @IsNotEmpty()
  @IsNumber()
  login: number;

  @IsNotEmpty()
  @IsString()
  password: string;
}
