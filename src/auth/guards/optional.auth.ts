import { Injectable } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

@Injectable()
export class OptionalAuth extends AuthGuard("jwt") {
  handleRequest(err, user, info) {
    return user;
  }
}
