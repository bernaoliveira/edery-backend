import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { IUser } from "../user/interfaces/user.inteface";
import { TokenService } from "src/token/token.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly tokenService: TokenService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request) => {
          return request?.headers?.authorization?.slice(7);
        },
      ]),
      secretOrKey: process.env.JWT_KEY,
      passReqToCallback: true,
    });
  }

  async validate(req, user: Partial<IUser>) {
    const tokenFromHeaders = req.headers.authorization.slice(7);
    const headerTokenExists = await this.tokenService.exists(
      user._id,
      tokenFromHeaders
    );

    if (headerTokenExists) {
      return user;
    } else {
      throw new UnauthorizedException();
    }
  }
}
