import { Response } from "express";
import { Body, Controller, Get, Post, Res, UseFilters } from "@nestjs/common";

import { AuthService } from "./auth.service";
import { SignInDto } from "./dto/signin.dto";
import { RedirectException } from "src/redirect/redirect.exception";
import { RedirectFilter } from "src/redirect/redirect.filter";

@Controller("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("login")
  async singIn(
    @Body() signInDto: SignInDto,
    @Res({ passthrough: true }) res: Response
  ) {
    return await this.authService.signIn(signInDto);
  }

  //TODO: refresh_token logic

  @Get("logout")
  @UseFilters(RedirectFilter)
  async logout(@Res() res) {
    throw new RedirectException("/auth");
  }
}
