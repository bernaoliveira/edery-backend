import {
  Injectable,
  MethodNotAllowedException,
  UnauthorizedException,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { SignOptions } from "jsonwebtoken";
import * as _ from "lodash";

import { UserService } from "src/user/user.service";
import { TokenService } from "src/token/token.service";
import { CreateUserTokenDto } from "src/token/dto/create-user-token.dto";
import { SignInDto } from "./dto/signin.dto";
import { IReadableUser } from "src/user/interfaces/readable-user.interface";
import { ITokenPayload } from "src/token/interfaces/token.payload";
import * as moment from "moment";
import { ApiService } from "../api/api.service";
import { CreateUserDto } from "../user/dto/create-user.dto";

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly tokenService: TokenService,
    private readonly userService: UserService,
    private readonly apiService: ApiService
  ) {}

  async signIn(signInDto: SignInDto): Promise<IReadableUser> {
    const eduData = await this.apiService.eduLogin(signInDto);

    let user;

    if (eduData.login == signInDto.login) {
      if (await this.userService.userExists(signInDto.login)) {
        await this.userService.updateUserByLogin(signInDto.login, {
          eduToken: eduData.eduToken,
        });
        user = await this.userService.findByLogin(eduData.login);
      } else {
        const userClass = await this.apiService.getUserClass(eduData.eduToken);
        user = await this.userService.create(
          _.assignIn(eduData, {
            password: signInDto.password,
            class: userClass,
          }) as CreateUserDto
        );
      }
    }

    const tokenPayload: ITokenPayload = {
      _id: user.id,
      roles: user.roles,
    };

    const token = await this.generateToken(tokenPayload);
    const expiresAt = moment().add(365, "day").toISOString();

    await this.saveToken({
      token,
      uid: user.id,
      expiresAt,
    });

    const newUser = { ...user.toObject() };
    delete newUser.password;
    const readableUser = newUser as IReadableUser;
    readableUser.token = token;

    return readableUser;
  }

  private async generateToken(
    data: string | object,
    options?: SignOptions
  ): Promise<string> {
    return this.jwtService.sign(data, options);
  }

  private async verifyToken(token): Promise<any> {
    try {
      const data = this.jwtService.verify(token);
      const tokenExists = await this.tokenService.exists(data._id, token);

      if (tokenExists) {
        return data;
      }
    } catch (error) {
      throw new UnauthorizedException();
    }
  }

  private async saveToken(createUserTokenDto: CreateUserTokenDto) {
    return await this.tokenService.create(createUserTokenDto);
  }

  public async getCookieWithJwtToken(
    data: string | object,
    options?: SignOptions
  ) {
    const token = await this.generateToken(data, options);
    return `Authentication=Bearer ${token}; HttpOnly; Path=/; Max-Age=${
      3600000 * 14 * 24
    }`;
  }
}
