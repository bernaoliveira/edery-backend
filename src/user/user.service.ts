import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import * as _ from "lodash";

import { IUser } from "./interfaces/user.inteface";
import { CreateUserDto } from "./dto/create-user.dto";

@Injectable()
export class UserService {
  constructor(@InjectModel("User") private readonly userModel: Model<IUser>) {}

  async create(createUserDto: CreateUserDto): Promise<IUser> {
    const createdUser = new this.userModel(
      _.assignIn(createUserDto, {
        roles: [],
      })
    );

    return await createdUser.save();
  }

  async findById(id: string): Promise<IUser> {
    return this.userModel.findById(id);
  }

  async findByLogin(login: number): Promise<IUser> {
    return this.userModel.findOne({ login });
  }

  async userExists(login: number): Promise<boolean> {
    return await this.userModel.exists({
      login,
    });
  }

  async updateUserByLogin(login: number, updateObject): Promise<boolean> {
    await this.userModel.updateOne({ login }, updateObject);
    return true;
  }
}
