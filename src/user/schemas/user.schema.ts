import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type SchemaDocument = User & Document;

@Schema()
export class User {
  @Prop({
    required: true,
  })
  eduToken: string;

  @Prop({
    required: true,
    unique: true,
  })
  login: number;

  @Prop({
    required: true,
  })
  name: string;

  @Prop({
    required: true,
  })
  password: string;

  @Prop({
    required: true,
  })
  school: string;

  @Prop({
    required: true,
  })
  class: string;

  @Prop({
    required: true,
  })
  roles: string[];
}

export const UserSchema = SchemaFactory.createForClass(User);
