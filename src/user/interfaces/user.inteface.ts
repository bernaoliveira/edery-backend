import { Document } from "mongoose";

export interface IUser extends Document {
  readonly login: number;
  readonly name: string;
  eduToken: string;
  password: string;
  readonly school: string;
  readonly class: string;
  readonly roles: string[];
}
