export interface IReadableUser {
  readonly login: number;
  readonly name: string;
  eduToken: string;
  readonly school: string;
  readonly class: string;
  readonly roles: string[];
  token?: string;
  password?: string;
}
