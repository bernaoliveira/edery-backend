import { Controller, Get, UseGuards } from "@nestjs/common";
import { UserService } from "./user.service";
import JwtGuard from "../auth/guards/jwt.guard";
import { User } from "./decorators/user.decorator";

@Controller("users")
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get("fetch")
  @UseGuards(JwtGuard)
  async getUser(@User() userToken) {
    return await this.userService.findById(userToken._id);
  }
}
